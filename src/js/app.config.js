let config = {
    planets : {
        Mercury : {
            diameter : "48.792",
            distance : "57.909.227",
            orbit :    "88"
        },
        Venus : {
            diameter : "12.104",
            distance : "108.209.475",
            orbit :    "225"
        },
        Earth : {
            diameter : "12.742",
            distance : "149.598.262",
            orbit :    "365"
        },
        Mars : {
            diameter : "6.779",
            distance : "227.943.824",
            orbit :    "693"
        },
        Jupiter : {
            diameter : "139.822",
            distance : "778.340.821",
            orbit :    "4.343"
        },
        Saturn : {
            diameter : "116.464",
            distance : "1.426.666.422",
            orbit :    "10.767"
        },
    },
    speeds : {
        Moon :    0.003,
        camera :  0.03,
        orbit_1 : 0.0002,
        orbit_2 : 0.0010,
        orbit_3 : 0.0030,
        orbit_4 : 0.0008,
        orbit_5 : 0.0015
    },
    meshes : [
        {
            name : 'Earth',
            texture : 'static/assets/earthmap2k.jpg',
            specular : 'static/assets/earthspec2k.jpg',
            normal : 'static/assets/earth_normalmap_flat2k.jpg',
            intersectable: true,
            size : 15,
            Xposition :'',
            transparent : false
        },
        {
            name : 'Clouds',
            texture : 'static/assets/fair_clouds_1k.png',
            specular : '',
            normal : '',
            intersectable: false,
            size : 15.1,
            Xposition :'',
            transparent: true
        },
        {
            name : 'Moon',
            texture : 'static/assets/moonmap2k.jpg',
            specular : '',
            normal : 'static/assets/moonbump2k.jpg',
            intersectable: true,
            size : 2,
            Xposition :'25',
            transparent: false
        },
        {
            name : 'Mercury',
            texture : 'static/assets/mercurymap.jpg',
            specular : '',
            normal : '',
            intersectable: true,
            size : 5,
            Xposition :'30',
            transparent: false
        },
        {
            name : 'Venus',
            texture : 'static/assets/venusmap.jpg',
            specular : '',
            normal : '',
            intersectable: true,
            size : 10,
            Xposition :'50',
            transparent: false
        },
        {
            name : 'Mars',
            texture : 'static/assets/mars_1k_color.jpg',
            specular : 'static/assets/mars_1k_topo.jpg',
            normal : 'static/assets/mars_1k_normal.jpg',
            intersectable: true,
            size : 10,
            Xposition :'120',
            transparent: false
        },
        {
            name : 'Jupiter',
            texture : 'static/assets/jupitermap.jpg',
            specular : '',
            normal : '',
            intersectable: true,
            size : 30,
            Xposition :'180',
            transparent: false
        },
        {
            name : 'Saturn',
            texture : 'static/assets/saturnmap.jpg',
            specular : '',
            normal : '',
            intersectable: true,
            size : 25,
            Xposition :'280',
            transparent: false
        }
    ]
};

export default config;