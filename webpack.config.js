module.exports = {
    entry : "./src/js/app.js",
    output : {
        path : "./",
        filename : "public/bundle.js"
    },
    devServer : {
        inline : true,
        port : 3333
    },
    devtool: "source-map", 
    module : {
        loaders : [
            {
                test : /\.jsx?$/,
                exclude : /node_modules/,
                loaders : ['babel']
            },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
            }
        ]
    }
}