## WebGL project

#### Project 

- Experimental project of a planetary system using Three.js framework for WebGL development
- Planets are clickable 
- GSAP library has been used for info-window animation

#### Run it !

````
- npm install
- npm run serve

serves in localhost:3333/

````

#### Demo

You can [see a demo here](http://arcemedia.no/planets)