/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _appRenderScene = __webpack_require__(1);
	
	var _appRenderScene2 = _interopRequireDefault(_appRenderScene);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	document.addEventListener("DOMContentLoaded", function () {
	     _appRenderScene2.default.init();
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _appConfig = __webpack_require__(2);
	
	var _appConfig2 = _interopRequireDefault(_appConfig);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var render = function () {
	
		// Config object
		var _config = _appConfig2.default;
	
		// Private vars
		var scene,
		    camera,
		    renderer,
		    raycaster,
		    cameraControl,
		    intersectableObjects = [],
		    tween = false,
		    mouse = new THREE.Vector2(),
		    INTERSECTED;
	
		// Meshes
		var meshesArray = [];
		var saturnRingsMesh, sunMesh;
	
		// Systems
		var mercurySystem, venusSystem, earthSystem, moonSystem, marsSystem, jupiterSystem, saturnSystem;
	
		// Exposed
		var init = initScene;
	
		/* Private functions  */
		function createRenderer() {
			renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
			renderer.setClearColor(0x000000, 1.0);
			renderer.setSize(window.innerWidth, window.innerHeight);
			renderer.shadowMap.enabled = true;
			renderer.setPixelRatio(window.devicePixelRatio);
	
			renderer.gammaInput = true;
			renderer.gammaOutput = true;
		}
	
		function createCamera() {
			camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 10000);
			camera.position.x = 14;
			camera.position.y = 10;
			camera.position.z = -122;
	
			cameraControl = new THREE.OrbitControls(camera);
			cameraControl.autoRotate = true;
			cameraControl.autoRotateSpeed = 0.04;
		}
	
		function createLight() {
			var ambientLight = new THREE.AmbientLight(0x03030303);
			scene.add(ambientLight);
		}
	
		function createMesh(mesh) {
			//Private vars from config file
			var _name = mesh.name,
			    _texture = mesh.texture,
			    _specular = mesh.specular,
			    _normal = mesh.normal,
			    _intersectable = mesh.intersectable,
			    _size = mesh.size,
			    _Xposition = mesh.Xposition,
			    _transparent = mesh.transparent;
	
			var sphereGeometry = new THREE.SphereGeometry(_size, 30, 30),
			    meshTexture = new THREE.Texture(),
			    loader = new THREE.ImageLoader(),
			    meshMaterial = new THREE.MeshLambertMaterial();
	
			loader.load(_texture, function (image) {
				meshTexture.image = image;
				meshTexture.needsUpdate = true;
			});
			meshMaterial.transparent = _transparent;
			meshMaterial.map = meshTexture;
			if (_specular) {
				var specularTexture = new THREE.Texture();
				loader = new THREE.ImageLoader();
				loader.load(_specular, function (image) {
					specularTexture.image = image;
					specularTexture.needsUpdate = true;
				});
				meshMaterial.specularMap = specularTexture;
				meshMaterial.specular = new THREE.Color(0x262626);
			}
			if (_normal) {
				var normalTexture = new THREE.Texture();
				loader = new THREE.ImageLoader();
				loader.load(_normal, function (image) {
					normalTexture.image = image;
					normalTexture.needsUpdate = true;
				});
				meshMaterial.normalMap = normalTexture;
				meshMaterial.normalScale = new THREE.Vector2(0.7, 0.7);
			}
	
			meshesArray[_name] = new THREE.Mesh(sphereGeometry, meshMaterial);
			meshesArray[_name].name = _name;
			meshesArray[_name].position.x = _Xposition;
			scene.add(meshesArray[_name]);
			if (_intersectable) {
				intersectableObjects.push(meshesArray[_name]);
			}
		}
		function createSaturnRings() {
			var radius = 25;
			var ringGeometry = new THREE.XRingGeometry(1.2 * radius, 2 * radius, 2 * 30, 5, 0, Math.PI * 2);
			var saturnRingsTexture = new THREE.Texture();
			var loader = new THREE.ImageLoader();
			loader.load('static/assets/saturnringcolor.jpg', function (image) {
				saturnRingsTexture.image = image;
				saturnRingsTexture.needsUpdate = true;
			});
			var saturnRingsMaterial = new THREE.MeshBasicMaterial();
			saturnRingsMaterial.map = saturnRingsTexture;
			saturnRingsMaterial.side = THREE.DoubleSide;
			saturnRingsMaterial.transparent = true;
			saturnRingsMaterial.opacity = 0.6;
	
			saturnRingsMesh = new THREE.Mesh(ringGeometry, saturnRingsMaterial);
			saturnRingsMesh.name = "SaturnRings";
			saturnRingsMesh.position.x = 280;
			saturnRingsMesh.rotation.x = 10 * Math.PI / 180;
	
			scene.add(saturnRingsMesh);
		}
		function createSystemsObjects() {
			mercurySystem = new THREE.Object3D();
			mercurySystem.add(meshesArray['Mercury']);
			mercurySystem.name = 'mercurySystem';
			scene.add(mercurySystem);
	
			venusSystem = new THREE.Object3D();
			venusSystem.add(meshesArray['Venus']);
			venusSystem.name = 'venusSystem';
			scene.add(venusSystem);
	
			moonSystem = new THREE.Object3D();
			moonSystem.add(meshesArray['Moon']);
			moonSystem.name = 'moonSystem';
			scene.add(moonSystem);
	
			earthSystem = new THREE.Object3D();
			earthSystem.name = 'earthSystem';
			meshesArray['Earth'].position.x = 0;
			meshesArray['Clouds'].position.x = 0;
			moonSystem.position.x = 0;
			meshesArray['Earth'].position.z = -90;
			meshesArray['Clouds'].position.z = -90;
			moonSystem.position.z = -90;
			earthSystem.add(meshesArray['Earth']);
			earthSystem.add(meshesArray['Clouds']);
			earthSystem.add(moonSystem);
			scene.add(earthSystem);
	
			marsSystem = new THREE.Object3D();
			marsSystem.add(meshesArray['Mars']);
			marsSystem.name = 'marsSystem';
			scene.add(marsSystem);
	
			jupiterSystem = new THREE.Object3D();
			jupiterSystem.add(meshesArray['Jupiter']);
			jupiterSystem.name = 'jupiterSystem';
			scene.add(jupiterSystem);
	
			saturnSystem = new THREE.Object3D();
			saturnSystem.add(meshesArray['Saturn']);
			saturnSystem.add(saturnRingsMesh);
			saturnSystem.name = 'saturnSystem';
			scene.add(saturnSystem);
		}
	
		function createSun() {
	
			var dirLight = new THREE.PointLight(0xffffff, 1);
			dirLight.position.set(0, 0, 0);
			dirLight.name = 'sun';
			scene.add(dirLight);
			/* dirLight.color.setHSL( 0.1, 0.7, 0.5 ); */
	
			var textureLoader = new THREE.TextureLoader();
			var textureFlare0 = textureLoader.load("static/assets/lensflare0.png");
			var textureFlare2 = textureLoader.load("static/assets/lensflare2.png");
			var textureFlare3 = textureLoader.load("static/assets/lensflare3.png");
	
			var flareColor = new THREE.Color(0xffffff);
			flareColor.setHSL(0.55, 0.9, 0.5 + 0.5);
	
			var lensFlare = new THREE.LensFlare(textureFlare0, 700, 0.0, THREE.AdditiveBlending, flareColor);
			lensFlare.name = 'lensFlare';
			lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
			lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
			lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
	
			lensFlare.add(textureFlare3, 60, 0.6, THREE.AdditiveBlending);
			lensFlare.add(textureFlare3, 70, 0.7, THREE.AdditiveBlending);
			lensFlare.add(textureFlare3, 120, 0.9, THREE.AdditiveBlending);
			lensFlare.add(textureFlare3, 70, 1.0, THREE.AdditiveBlending);
	
			/* 	lensFlare.position.set( 0, 0, -50 );	 */
			lensFlare.position.copy(dirLight.position);
	
			scene.add(lensFlare);
		}
		function createStarField() {
			var envGeometry = new THREE.SphereGeometry(9000, 32, 32);
	
			var envTexture = new THREE.Texture();
			var loader = new THREE.ImageLoader();
			loader.load('static/assets/galaxy_starfield.png', function (image) {
				envTexture.image = image;
				envTexture.needsUpdate = true;
			});
	
			var envMaterial = new THREE.MeshBasicMaterial();
			envMaterial.map = envTexture;
			envMaterial.side = THREE.BackSide;
	
			var meshEnv = new THREE.Mesh(envGeometry, envMaterial);
			meshEnv.name = 'meshEnv';
			scene.add(meshEnv);
		}
		function onWindowResize(event) {
			renderer.setSize(window.innerWidth, window.innerHeight);
			camera.aspect = window.innerWidth / window.innerHeight;
			camera.updateProjectionMatrix();
		}
	
		function createHelpers() {
			var helper = new THREE.GridHelper(200, 20);
			helper.setColors(0xffffff);
			helper.position.y = 0;
			scene.add(helper);
	
			var dir = new THREE.Vector3(1, 0, 0);
			var origin = new THREE.Vector3(0, 0, 0);
			var length = 50;
			var hex = 0xffff00;
			var arrowHelper = new THREE.ArrowHelper(dir, origin, length, hex);
			scene.add(arrowHelper);
	
			var dir = new THREE.Vector3(0, 1, 0);
			var origin = new THREE.Vector3(0, 0, 0);
			var length = 50;
			var hex = 0x00ff00;
			var arrowHelper = new THREE.ArrowHelper(dir, origin, length, hex);
			scene.add(arrowHelper);
	
			var dir = new THREE.Vector3(0, 0, 1);
			var origin = new THREE.Vector3(0, 0, 0);
			var length = 50;
			var hex = 0x0000ff;
			var arrowHelper = new THREE.ArrowHelper(dir, origin, length, hex);
			scene.add(arrowHelper);
		}
		function onDocumentMouseMove(event) {
			var intersects, raycaster;
	
			event.preventDefault();
			mouse.x = event.clientX / window.innerWidth * 2 - 1;
			mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
	
			//Raycaster functionality track Mesh by mouseover
			raycaster = new THREE.Raycaster();
			raycaster.setFromCamera(mouse, camera);
			intersects = raycaster.intersectObjects(intersectableObjects, true);
	
			if (intersects.length > 0) {
				//On MouseIn							
				container.style.cursor = "pointer";
			} else {
				//On MouseOut
				container.style.cursor = "default";
			}
		}
		function onDocumentMouseClick(event) {
			var intersects, raycaster;
	
			event.preventDefault();
			mouse.x = event.clientX / window.innerWidth * 2 - 1;
			mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
	
			//Raycaster functionality track Mesh by mousedown
			raycaster = new THREE.Raycaster();
			raycaster.setFromCamera(mouse, camera);
			intersects = raycaster.intersectObjects(intersectableObjects, true);
	
			if (intersects.length > 0) {
				//On MouseIn	
				if (INTERSECTED != intersects[0].object) {
					INTERSECTED = intersects[0].object;
					createInfowindow(INTERSECTED.name);
				}
			}
		}
		function cleanInfowindow() {
			var info_window_old;
			if (document.querySelector(".info-window")) {
				info_window_old = document.querySelector(".info-window");
				TweenLite.to(info_window_old, 0.6, { opacity: 0, onComplete: removeElement, onCompleteParams: [info_window_old] });
			}
		}
		function removeElement(elem) {
			elem.parentNode.removeChild(elem);
		}
		function createInfowindow(name) {
			cleanInfowindow();
	
			var imgpath = "static/assets/infowindow/" + name + ".png",
			    info_window = document.createElement('div'),
			    img = document.createElement("img"),
			    text = document.createElement("div"),
			    text2 = document.createElement("div"),
			    text3 = document.createElement("div"),
			    text4 = document.createElement("div");
	
			img.classList.add("image-planet");
			img.setAttribute("src", imgpath);
			img.setAttribute("alt", name);
			img.setAttribute("width", "200px");
	
			text.innerHTML = "<p class='name'>" + name + "</p>";
			text.style.color = "#f4f1f1";
			text.style['text-transform'] = "uppercase";
			text.style['padding-left'] = "45px";
	
			if (_config.planets[name]) {
				text2.innerHTML = "<p>Diameter: <span>" + _config.planets[name].diameter + " km</span></p>";
				text3.innerHTML = "<p>Sun distance: <span>" + _config.planets[name].distance + " km</span></p>";
				text4.innerHTML = "<p>Orbit period: <span> " + _config.planets[name].orbit + " days</span></p>";
			}
	
			info_window.classList.add("info-window");
			info_window.appendChild(img);
			info_window.appendChild(text);
			info_window.appendChild(text2);
			info_window.appendChild(text3);
			info_window.appendChild(text4);
			document.body.appendChild(info_window);
	
			TweenLite.from(info_window, 0.7, { top: "-100%", delay: 0.4 });
		}
	
		function render() {
	
			cameraControl.update();
	
			//movement
			meshesArray['Earth'].rotation.y += _config.speeds.orbit_4;
			meshesArray['Clouds'].rotation.y += _config.speeds.orbit_2;
			moonSystem.rotation.y += _config.speeds.Moon;
			earthSystem.rotation.y += _config.speeds.orbit_1;
			mercurySystem.rotation.y += _config.speeds.orbit_5;
			venusSystem.rotation.y += _config.speeds.orbit_4;
			marsSystem.rotation.y += _config.speeds.orbit_2;
			jupiterSystem.rotation.y += _config.speeds.orbit_3;
			saturnSystem.rotation.y += _config.speeds.orbit_5;
	
			camera.position.z -= _config.speeds.camera;
	
			// 	Animate 
			renderer.render(scene, camera);
			requestAnimationFrame(render);
		}
	
		/* Public Functions */
		function initScene() {
			scene = new THREE.Scene();
	
			// Create all planets
			_config.meshes.forEach(function (mesh) {
				createMesh(mesh);
			});
	
			createRenderer();
			createCamera();
			createLight();
			createSun();
			createStarField();
			createSaturnRings();
			createSystemsObjects();
	
			var container = document.getElementById("container");
			container.appendChild(renderer.domElement);
	
			document.addEventListener('mouseup', onDocumentMouseClick, false);
			document.addEventListener('mousemove', onDocumentMouseMove, false);
			window.addEventListener('resize', onWindowResize, false);
	
			render();
		}
	
		return {
			init: init
		};
	}();
	
	exports.default = render;

/***/ },
/* 2 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var config = {
	    planets: {
	        Mercury: {
	            diameter: "48.792",
	            distance: "57.909.227",
	            orbit: "88"
	        },
	        Venus: {
	            diameter: "12.104",
	            distance: "108.209.475",
	            orbit: "225"
	        },
	        Earth: {
	            diameter: "12.742",
	            distance: "149.598.262",
	            orbit: "365"
	        },
	        Mars: {
	            diameter: "6.779",
	            distance: "227.943.824",
	            orbit: "693"
	        },
	        Jupiter: {
	            diameter: "139.822",
	            distance: "778.340.821",
	            orbit: "4.343"
	        },
	        Saturn: {
	            diameter: "116.464",
	            distance: "1.426.666.422",
	            orbit: "10.767"
	        }
	    },
	    speeds: {
	        Moon: 0.003,
	        camera: 0.03,
	        orbit_1: 0.0002,
	        orbit_2: 0.0010,
	        orbit_3: 0.0030,
	        orbit_4: 0.0008,
	        orbit_5: 0.0015
	    },
	    meshes: [{
	        name: 'Earth',
	        texture: 'static/assets/earthmap2k.jpg',
	        specular: 'static/assets/earthspec2k.jpg',
	        normal: 'static/assets/earth_normalmap_flat2k.jpg',
	        intersectable: true,
	        size: 15,
	        Xposition: '',
	        transparent: false
	    }, {
	        name: 'Clouds',
	        texture: 'static/assets/fair_clouds_1k.png',
	        specular: '',
	        normal: '',
	        intersectable: false,
	        size: 15.1,
	        Xposition: '',
	        transparent: true
	    }, {
	        name: 'Moon',
	        texture: 'static/assets/moonmap2k.jpg',
	        specular: '',
	        normal: 'static/assets/moonbump2k.jpg',
	        intersectable: true,
	        size: 2,
	        Xposition: '25',
	        transparent: false
	    }, {
	        name: 'Mercury',
	        texture: 'static/assets/mercurymap.jpg',
	        specular: '',
	        normal: '',
	        intersectable: true,
	        size: 5,
	        Xposition: '30',
	        transparent: false
	    }, {
	        name: 'Venus',
	        texture: 'static/assets/venusmap.jpg',
	        specular: '',
	        normal: '',
	        intersectable: true,
	        size: 10,
	        Xposition: '50',
	        transparent: false
	    }, {
	        name: 'Mars',
	        texture: 'static/assets/mars_1k_color.jpg',
	        specular: 'static/assets/mars_1k_topo.jpg',
	        normal: 'static/assets/mars_1k_normal.jpg',
	        intersectable: true,
	        size: 10,
	        Xposition: '120',
	        transparent: false
	    }, {
	        name: 'Jupiter',
	        texture: 'static/assets/jupitermap.jpg',
	        specular: '',
	        normal: '',
	        intersectable: true,
	        size: 30,
	        Xposition: '180',
	        transparent: false
	    }, {
	        name: 'Saturn',
	        texture: 'static/assets/saturnmap.jpg',
	        specular: '',
	        normal: '',
	        intersectable: true,
	        size: 25,
	        Xposition: '280',
	        transparent: false
	    }]
	};
	
	exports.default = config;

/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map